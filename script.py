from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from decouple import config

# Bigram
import nltk;


PATH = config('DRIVER_PATH')
driver = webdriver.Chrome(PATH)

driver.get("https://www.cnas.org/articles-multimedia")


try:
    contents = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "content"))
    )
    Contents = contents.find_elements_by_class_name("-with-image")
    # Loop for multiple artcile on same page
    for eachContent in Contents:
        eachContent.find_element_by_css_selector(
            '#content > section > ul > li:nth-child(1) > a').click()
        article = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "mainbar"))
        )
        paragraphs = article.find_elements_by_tag_name('p')
        print(article.text)
        driver.back()


except:
    driver.quit()
